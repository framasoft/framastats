<?php
// ============================================================
// Script called very often by cronjob :
// Calculate and provide in HTML files all stats
// needed in framastats.org
//
// Permits to be a lot more efficient when called framastats.org
// because we just retrieve the latest stats already calculated
// =============================================================

$nameHTMLFile 		= "calculatedStats.html";
$nameHTMLFile_tmp	= "calculatedStats_tmp";
$nameJSONFile_Chart	= "calculatedChartsStats.html";
$nameJSONFile_Chart_tmp	= "calculatedChartsStats_tmp";

// Calculate stats, put the results in a temporary file and when all calculation is finished, replace the file used for the website
$createStats		= "(php calculateStats.php) > ../../admin/" . $nameHTMLFile_tmp;
$replaceFormerStats	= "mv ../../admin/" . $nameHTMLFile_tmp . " ../../admin/" . $nameHTMLFile;
shell_exec($createStats);
shell_exec($replaceFormerStats);

// The same but for charts stats
$createStats_c		= "(php calculateChartsStats.php) > ../../admin/" . $nameJSONFile_Chart_tmp;
$replaceFormerStats_c	= "mv ../../admin/" . $nameJSONFile_Chart_tmp . " ../../admin/" . $nameJSONFile_Chart;
shell_exec($createStats_c);
shell_exec($replaceFormerStats_c);

?>
