<?php
// ===============================================================
// Script called by index.php to retrieve already calculated stats
// ===============================================================
$urlAdminWebFolder		= "http://framastats.org/admin/";
$nameHTMLFile			= "calculatedStats";
$nameJSONFile_ChartFile 	= "calculatedChartsStats";

$urlCalculatedStats 		= $urlAdminWebFolder . $nameHTMLFile . ".html";
$urlCalculatedStats_Charts 	= $urlAdminWebFolder . $nameJSONFile_ChartFile . ".html";
$res_Stats			= file_get_contents($urlCalculatedStats);
$res_Stats_Charts		= file_get_contents($urlCalculatedStats_Charts);

// Cancel loading if one file is not responding
if ($res_Stats !== false || $res_Stats_Charts !== false) {
	
	echo $res_Stats;
	echo $res_Stats_Charts;

} else {
	echo "Problem while loading stats. Please wait while we're fixing the problem.";
}

?>
