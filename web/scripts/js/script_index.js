//When DOM loaded
$(document).ready(function() {
	
	////////////////////////////////
	/////////// AJAX CALLS /////////
	////////////////////////////////
	
	/*
	 * Get stats
	 * Every 1 minute
	 * 
	 * */
	 setTimeout(getStats,0); // Do it one time
	 setInterval(getStats,60000); // Repeat it
	 
	 function getStats() {
		// 1. Creating XHR object
		var xhr = getXMLHttpRequest();

		// 3. Receiving response
		xhr.onreadystatechange=function() {
			if (xhr.readyState==4 && xhr.status==200) {
				var stats = xhr.responseText;
				$("#getStats").html(stats);
				fillDivStats();
			}
		}

		// 2. Sending Request
		xhr.open("GET","scripts/php/retrieveCalculatedStats.php",true);
		xhr.send();

	 }

	////////////////////////////////
	////////////// MAIN ////////////
	////////////////////////////////
	
	function fillDivStats() {
		// ############ Fill div with stats (that are in div getStats ############
		$( "#getStats span" ).each(function(){

			// Retrieve each stat
			var nameStat = $(this).attr('class');
			var valueStat = $(this).html();

			// Fill div title with the same class name
			var selector = ".statToFill." + nameStat;
			$(selector).html(valueStat);
		});
	}

	// ########### Change iconDisplay if panel is hidden or shown ############
	$( ".panel-collapse.collapse.withChevron" ).on('hidden.bs.collapse', function(){
		selectorIcon = "a[href='#" + $(this).attr('id') + "']"; // retrive panel that I've been collapse
		$(selectorIcon).find('.glyphicon').attr('class', 'glyphicon glyphicon-chevron-down');
		$(selectorIcon).parent()
				.mouseenter(function() {
				  $(this).css("background-color","#e0e0e0");
				})
				.mouseleave(function() {
				  $(this).css("background-color","inherit");
				});
	});
	$( ".panel-collapse.collapse.withChevron" ).on('shown.bs.collapse', function(){
		selectorIcon = "a[href='#" + $(this).attr('id') + "']";
		$(selectorIcon).find('.glyphicon').attr('class', 'glyphicon glyphicon-chevron-up');
		$(selectorIcon).parent()
				.mouseenter(function() {
				  $(this).css("background-color","#e0e0e0");
				})
				.mouseleave(function() {
				  $(this).css("background-color","inherit");
				});
	});
	
	$( ".panel-heading" ).click(function() {
		var selector = $(this).attr('link');
		$(selector).click();
	});

	////////////////////////////////
	/////// SPECIAL FUNCTIONS //////
	////////////////////////////////
		
	/*
	 * Create and return a XHR Object
	 * 
	 * */
	function getXMLHttpRequest() {
		var xhr = null;
		
		if (window.XMLHttpRequest || window.ActiveXObject) {
			if (window.ActiveXObject) {
				try {
					xhr = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {
					xhr = new ActiveXObject("Microsoft.XMLHTTP");
				}
			} else {
				xhr = new XMLHttpRequest(); 
			}
		} else {
			alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
			return null;
		}
		
		return xhr;
	}

});
