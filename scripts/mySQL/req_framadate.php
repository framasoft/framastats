<?php

/*
 * include by mySQL/mainScript.php
 *
 * Create database framastats_pulls (day/stat/removed)
 * Create $stats
 *
 *
 * */

// ################### CONSTANTS ##################
$totalNbPulls_start 	= 0;
$tableStatsPulls 	= 'framastats_pulls';
$tableStatsPulls_date 	= 'day';
$tableStatsPulls_removed= 'removed';
$tablePulls 		= 'fd_poll';
$tablePulls_date	= 'creation_date';
$tablePulls_end_date	= 'end_date';
$tableVote		= 'fd_vote';
$tableVote_id_poll	= 'poll_id';


// #################### DATABASE ##################
$fields = array (
	'stat_ID' => 'INT NOT NULL AUTO_INCREMENT',
	'day'=> 'DateTime NOT NULL',
	'stat' => 'INT NOT NULL',
	'removed' => 'INT NOT NULL'
	);

// Create DbTable
$sql = Util_mySQL::createDbtable($dBase, $tableStatsPulls, $fields);
if($sql == -1) {
	$util->out("## Error in creating $tableStatsPulls", 'error', true); // exit
} else {
	// $util->out("### $sql");
}


// ################### REQUESTS ##################

// *** Numbers of pulls
$res = $dBase->query("SELECT COUNT(*) FROM $tablePulls");
$nbPulls_all_living =  $res->fetch();
$res->closeCursor();

$res = $dBase->query("SELECT COUNT(*) FROM $tablePulls WHERE DATE($tablePulls_date)=CURDATE()");
$nbPulls_today =  $res->fetch();
$res->closeCursor();

// *** Kinds and percentages of pulls
$res = $dBase->query("SELECT s.format AS format, COUNT(*) AS nb, COUNT(*) / T.total * 100 AS percent
			FROM $tablePulls as s, (SELECT COUNT(*) AS total FROM $tablePulls) AS T
			GROUP BY format
			ORDER BY percent DESC");

while ($data = $res->fetch())
{
	$tmpArray = array($data['format'] => $data['percent']);
	$array_formatPercent[] = $tmpArray;
}
$res->closeCursor();

// *** Life Expectancy of pulls (cast)
$res = $dBase->query("SELECT AVG(duration) FROM (
			SELECT DATEDIFF($tablePulls_end_date, $tablePulls_date) as duration
			FROM $tablePulls
			HAVING duration > 0) S1");
$pulls_lifeExpectancy =  $res->fetch()['AVG(duration)'];
$res->closeCursor();

// *** How many users per pulls
$res = $dBase->query("SELECT AVG(nb) FROM (
			SELECT COUNT(*) as nb
			FROM $tableVote
			GROUP BY $tableVote_id_poll) AS S1");
$pulls_avgNbUsers =  $res->fetch()['AVG(nb)'];
$res->closeCursor();



// ##################### STATS ####################
$stats->mySQL['site'] 				= "Framadate";
$stats->mySQL['timeUpdateStats']		= date('Y-m-d H:i:s');

// *** Numbers of pulls living
$stats->mySQL['nbPulls_all_living'] 		= (int) $nbPulls_all_living['COUNT(*)'];
$stats->mySQL['nbPulls_1lastYear_living']	= Util_mySQL::countSinceXDays($dBase, $tablePulls, $tablePulls_date, '1 YEAR');
$stats->mySQL['nbPulls_6lastMonths_living']	= Util_mySQL::countSinceXDays($dBase, $tablePulls, $tablePulls_date, '6 MONTH');
$stats->mySQL['nbPulls_3lastMonths_living']	= Util_mySQL::countSinceXDays($dBase, $tablePulls, $tablePulls_date, '3 MONTH');
$stats->mySQL['nbPulls_1lastMonth_living']	= Util_mySQL::countSinceXDays($dBase, $tablePulls, $tablePulls_date, '1 MONTH');
$stats->mySQL['nbPulls_1lastWeek_living']	= Util_mySQL::countSinceXDays($dBase, $tablePulls, $tablePulls_date, '1 WEEK');
$stats->mySQL['nbPulls_today_living']		= Util_mySQL::countSinceXDays($dBase, $tablePulls, $tablePulls_date, '0 DAY');

// Handle pulls that can be deleted and created : stats_created
$stats = Util_mySQL::dealWithStats($stats, $dBase, $tableStatsPulls, $tmp_pathFileName, $totalNbPulls_start, $stats->mySQL['nbPulls_today_living'], $tablePulls, $tablePulls_date, 'nbPulls', $tableStatsPulls, 'day', 'stat', 'removed');

// *** Removed pulls (not possible first time)
if (file_exists($tmp_pathFileName)) {

	$tmp_removed = $stats->mySQL['nbPulls_today_created'] - $stats->mySQL['nbPulls_today_living'];

	if ($tmp_removed < 0) { // may happen because of changing days..
		$util->out("### More living-pulls from today than created-pulls (difference = " . $tmp_removed . "). Check script.");
		$tmp_removed = 0;
	}
	$stats->mySQL['nbPulls_today_removed']		= $tmp_removed;

	// Insert in dBase
	Util_mySQL::handleInsertOrUpdate ($dBase,$tableStatsPulls, date("Y-m-d"), $tableStatsPulls_removed, $tmp_removed);

	// Retrieve stats
	$stats->mySQL['nbPulls_1lastMonth_removed']	= Util_mySQL::sumSinceXDays($dBase, $tableStatsPulls, $tableStatsPulls_removed, $tableStatsPulls_date, '1 MONTH');
	$stats->mySQL['nbPulls_1lastWeek_removed']	= Util_mySQL::sumSinceXDays($dBase, $tableStatsPulls, $tableStatsPulls_removed, $tableStatsPulls_date, '1 WEEK');
} else {
	$util->out("### Temporary stats file has never been created. ", 'info');
}

// *** Kinds and percentages of pulls
$stats->mySQL['pulls_formats']			= $array_formatPercent;

// *** Life Expectancy of pulls
$stats->mySQL['pulls_lifeExpectancyInDays']	= $pulls_lifeExpectancy;

// *** How many users per pulls
$stats->mySQL['pulls_avgNbUsers']		= $pulls_avgNbUsers;

?>
