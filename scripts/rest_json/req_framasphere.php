<?php

/*
 * include by rest_json/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$a_urlStat = 'https://framasphere.org/nodeinfo/2.0';
$toFindStat	= 'statistic';
$TOTAL_USERS	= 'Total users';
$TOTAL_POSTS	= 'Local posts';
$TOTAL_COMMENTS	= 'Local comments';

// ##################### STATS ####################
$stats->rest_json['site']                       = "Framasphere";
$stats->rest_json['timeUpdateStats']            = date('Y-m-d H:i:s');

// ################ REQUESTS AND STATS ############

if (!file_exists($tmp_pathFileName)) {
        $util->out("### (req_framasphere) Temporary stats file has never been created. ", "info");
}

// Retrieve stat framasphere
$json = file_get_contents($a_urlStat);
$obj = json_decode($json, true);
//var_dump($obj);

// ##################### STATS ####################
$stats->rest_json['total_users'] = $obj['usage']['users']['total'];
$stats->rest_json['total_users_a_hy'] = $obj['usage']['users']['activeHalfyear'];
$stats->rest_json['total_users_a_mo'] = $obj['usage']['users']['activeMonth'];

//var_dump($stats);

?>
