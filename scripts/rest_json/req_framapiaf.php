<?php

/*
 * include by rest_json/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$a_urlStat = 'https://framapiaf.org/about/more';
$toFindStat= 'information-board__section';

// ##################### STATS ####################
$stats->rest_json['site'] 			= "Framapiaf";
$stats->rest_json['timeUpdateStats']		= date('Y-m-d H:i:s');

// ################ REQUESTS AND STATS ############

if (!file_exists($tmp_pathFileName)) {
	$util->out("### (req_framapiaf) Temporary stats file has never been created. ", "info");
}

// Retrieve html file
$doc = new DOMDocument;
libxml_use_internal_errors(true);
$doc->loadHTMLFile($a_urlStat);
libxml_clear_errors();

// Get all div
$tags = $doc->getElementsByTagName('div');

// Find and put in array
$res = array();

foreach ($tags as $tag) {

	// Search in div 'section'
	if ( $tag->getAttribute('class') == "$toFindStat" ) {
		$match = $tag->nodeValue;

		$separator = "\n";
		$line = strtok($match, $separator);

		// In each 'section', search for numbers and put in $matches
		while ($line !== false) {
			// var_dump($line);
			if ( preg_match('/[0-9 ]+[0-9]/m', $line, $match, PREG_OFFSET_CAPTURE) ) {
				// replace comas and spaces  with nothing
				$strToNb  = str_replace(',','',$match[0][0]);
				$strToNb2 = str_replace(' ','',$strToNb);
				$res[] = $strToNb2;
			}

			$line = strtok( $separator );
		}
	}
}


// ##################### STATS ####################
if (isset($res[0])) {
	$stats->rest_json['users'] = $res[0];
	if (isset($res[1])) {
		$stats->rest_json['posts'] = $res[1];
		if (isset($res[2])) {
			$stats->rest_json['connectedNods'] = $res[2];
		}
	}
}
//var_dump($stats);
?>
