<?php

/*
 * include by dispatcher.php
 *
 * Retrieve $stats
 *
 * */

// ################## INCLUDE #####################
//$pathToUtil = "Util_bash_commands.php";
//include ($pathToUtil);


// ################# CONSTANTS #################
$pathReqMyFrama	= "req_myframa.php";

// ############## MAIN SCRIPT ###################
$util->out("### Stats with bash_commands", "info");

// ################### STATS #####################

// add values to $stats which is an instance of Stats
switch ($u_service) {
	case "myframa":
		include($pathReqMyFrama);
		break;
	default:
		$stats->site = "default site name";
		$util->out("### This service is not initialized for bash_commands", "error", true); //exit
		break;
}
?>
