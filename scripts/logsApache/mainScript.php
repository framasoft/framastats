<?php

/*
 * include by dispatcher.php
 * 
 * Handle with finding logs
 * Redirect request according to the service.
 * Retrieve $stats
 * 
 * */

// ################## INCLUDE #####################
$pathToUtil_LogsApache = "Util_logsApache.php";
include ($pathToUtil_LogsApache);
 
// ################## CONSTANTS ###################
$pathToLogsFolder_Framadvd 	= "../../../log/"; // warning, files are included
$pathToLogsFolder_Framalibre	= "../../../log/";
$pathToLogsFolder_Framabin	= "../../../log/";
$pathToLogsFolder		= "";

$regex_fileLogs 		= "/access.log/";

$pathReqFramadvd 		= "req_framadvd.php";
$pathReqFramalibre 		= "req_framalibre.php";
$pathReqFramabin 		= "req_framabin.php";

// ################### SCRIPT #####################
$util->out("### Stats with Logs Apache", "info");

switch ($u_service) {
	case "framadvd":
		$pathToLogsFolder = $pathToLogsFolder_Framadvd;
		break;
	case "framalibre":
		$pathToLogsFolder = $pathToLogsFolder_Framalibre;
		break;
	case "framabin":
		$pathToLogsFolder = $pathToLogsFolder_Framabin;
		break;
	default:
		$util->out("### This service is not initialized for logsApache", "error", true); //exit
		break;
}

// Find all logs files
$arrayLogsFiles = scandir ($pathToLogsFolder);
$s_logsFile 	= json_encode($arrayLogsFiles);

// Search for logs and put in $a_resLogs
if (FALSE == preg_match_all($regex_fileLogs, $s_logsFile, $a_resLogs))
{
	$util->out("### Doesn't find file log !", "error", true); //exit
}
else
{
	// Yesterday log :
	// $pathToFileLog = $pathToLogsFolder . substr($a_resLogs[0][0], 0, -1); 

	// Today log
	$pathToFileLog = $pathToLogsFolder . $a_resLogs[0][0];
	
}

// Open logs file ($pathToFileLog) et search for downloads
$a_logs = file($pathToFileLog);
if ($a_logs == FALSE)
{
	$util->out("### Problem while reading " . $pathToFileLog, "error", true); //exit
}
else
{
	// A first switch is above
	switch ($u_service) {
		case "framadvd":
			// create Stats 
			include($pathReqFramadvd); 
			break;
		case "framalibre":
			// create Stats 
			include($pathReqFramalibre); 
			break;
		case "framabin":
			// create Stats 
			include($pathReqFramabin); 
			break;
		default:
			$util->out("### This service is not initialized for logsApache", "error", true); //exit
			break;
	}
}

?>
