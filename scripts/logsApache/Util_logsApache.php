<?php

class Util_logsApache
{
	/*
	 * Handle stats with @_logs
	 * 	- Count all lines corresponding to @_regex 
	 * 	- Remove logs for an array which don't fit our requirements (see Util_logsApache::removeFalsePositive)
	 * 
	 * @_logs : logs content
	 * @_regex : regex for catching the good lines
	 * @_nameStat : name of the stats we're looking for (informal)
	 * @_regex_control : regex with capturing groups (see Constants of your script)
	 * @_statusCode we accepte
	 * @_minBytesDownload possible
	 * return the count of lines of logs corresponding and respecting rules
	 * */
	public static function dealsWithStats ($util, $_logs, $_regex, $_nameStat = 'visits', $_regex_control = null, $_statusCode = null, $_minBytesDownload = null, $_referers = null) {
		// *** Get all lines corresponding to the regex ***
		$a_logsCorresponding = preg_grep($_regex, $_logs);
		
		if ($_regex_control != null && $_statusCode != null && $_minBytesDownload != null) {
			
			// Delete false positive visits
			$tmpNumber 		= count($a_logsCorresponding); 
			$a_logsCorresponding 	= Util_logsApache::removeFalsePositive($a_logsCorresponding, $_regex_control, $_statusCode, $_minBytesDownload);
			$finaltmpNumber 	= count($a_logsCorresponding); 
		
			// Display for debug
			$nbOfDelete = $tmpNumber - $finaltmpNumber;
			$util->out("### Remove $nbOfDelete $_nameStat after checking", "info");
			$util->out("### Final number of $_nameStat : $finaltmpNumber", "info");
			
			return $finaltmpNumber;
		} else {
			return count($a_logsCorresponding);
		}
	}

	/*
	 * Remove logs for an array which don't fit our requirements
	 * @_arrayLogs : array with logs
	 * @_regex_control : regex with capturing groups (see Constants of your script)
	 * @_statusCode we accept
	 * @_minBytesDownloaded
	 * @_referers possible
	 * return _arrayLogs after removing false positive
	 * */
	public static function removeFalsePositive($_arrayLogs, $_regex_control, $_statusCode, $_minBytesDownloaded, $_referers = null)
	{
		foreach ($_arrayLogs as $log_key => $log_value)
		{
			
			if (1 == preg_match($_regex_control, $log_value, $matches))
			{
				// Check status code
				if ($matches[3] != $_statusCode)
				{
					unset ($_arrayLogs[$log_key]);
					continue;
				}

				// Check that the user really download the file
				if ($matches[4] < $_minBytesDownloaded)
				{
					unset ($_arrayLogs[$log_key]);
					continue;
				}

				// Check the site that the client reports having been referred from
				if ($_referers != null)
				{
					if (!in_array($matches[5], $_referers))
					{
						unset ($_arrayLogs[$log_key]);
						continue;
					}
				}

				// Check that the client is not a bot
				if (1 == preg_match("/bot/i", $matches[6]))
				{
					unset ($_arrayLogs[$log_key]);
					continue;
				}
			}
		}

		return $_arrayLogs;
	}

	/*
	 * Handle visits-stats coming few times in the same day
	 * @tmp_pathFileName : path of tmp stats file
	 * @_totalStart : arbitrary number of visit
	 * @_stats_today : number of daily visits
	 * @_logDate : permits to know if we already make the stats
	 * return $stats
	 * */
	public static function dealWithStats ($stats, $tmp_pathFileName, $_totalStart, $_stats_today, $_logDate, $_nameStats = 'visits')
	{
		global $util;
		$_nameStats_today	= $_nameStats . '_today';
		$_nameStats_all 	= $_nameStats . '_all';

		if (!file_exists($tmp_pathFileName))
		{
			$util->out("### Temporary stats file has never been created yet. ", "info");
			$stats = Util_logsApache::createStats_init($stats, $_totalStart, $_stats_today, $_logDate, $_nameStats_today, $_nameStats_all);
		}
		else // file exists
		{
			// Retrieve stats
			$arrayStatsLogs = json_decode(file_get_contents($tmp_pathFileName), true);
			$_lastDayUpdateStats = $arrayStatsLogs['logs']['lastDayUpdateStats'];
			$_firstDayOfStats = $arrayStatsLogs['logs']['firstDayOfStats'];

			if ($_lastDayUpdateStats != $_logDate) // First update of the day
			{
				$visitsAll = $arrayStatsLogs['logs'][$_nameStats_all] + $_stats_today;
				$stats = Util_logsApache::createStats($stats, $visitsAll, $_stats_today, $_logDate, $_firstDayOfStats, $_nameStats_today, $_nameStats_all);
			}
			else // Script already launched today : we don't count the same download twice
			{
				$visitsAll = $arrayStatsLogs['logs'][$_nameStats_all] - $arrayStatsLogs['logs'][$_nameStats_today];
				$visitsAll += $_stats_today;
				$stats = Util_logsApache::createStats($stats, $visitsAll, $_stats_today, $_logDate, $_firstDayOfStats, $_nameStats_today, $_nameStats_all);
			}
		}

		return $stats;
	}

	/*
	 * Called by dealWithStats
	 * */
	private static function createStats_init ($stats, $_totalStart, $_stats_today, $_logDate, $_nameStats_today = 'visits_today', $_nameStats_all = 'visits_all')
	{
		$stats->logs['timeUpdateStats'] 		= date('Y-m-d H:i:s');
		$stats->logs['firstDayOfStats'] 		= date('Y-m-d');
		$stats->logs['lastDayUpdateStats'] 		= $_logDate;
		$stats->logs[$_nameStats_today]			= $_stats_today;
		$stats->logs[$_nameStats_all]			= $_totalStart + $stats->logs[$_nameStats_today];

		return $stats;
	}

	/*
	 * Called by dealWithStats
	 * */
	private static function createStats ($stats, $_visits_all, $_stats_today, $_logDate, $_firstDayOfStats, $_nameStats_today = 'visits_today', $_nameStats_all = 'visits_all')
	{
		$stats->logs['timeUpdateStats'] 	= date('Y-m-d H:i:s');
		$stats->logs['firstDayOfStats'] 	= $_firstDayOfStats;
		$stats->logs['lastDayUpdateStats']	= $_logDate;
		$stats->logs[$_nameStats_today]		= $_stats_today;
		$stats->logs[$_nameStats_all]		= $_visits_all;

		return $stats;
	}

	/*
	 * Not used anymore
	 * Called by dealWithStats
	 * */
	private static function createStats_lastXdays ($XDays, $changeDay, $stats, $_arrayStatsLogs, $_nameStats_today, $_nameStats_lastWeek) {

		$_nameStats_lastWeek_History	= $_nameStats_lastWeek . '_history';
		$tmp_lastWeek 			= $_arrayStatsLogs['logs'][$_nameStats_lastWeek_History];

		if ($changeDay) {
			// Retrieve yesterday stats
			$_yesterdayStat = $_arrayStatsLogs['logs'][$_nameStats_today];

			// Handle stack with last XDays stats
			if (count($tmp_lastWeek) >= $XDays) {
				// remove the first stat
				array_shift($_arrayStatsLogs['logs'][$_nameStats_lastWeek_History]);
				// add a stat onto the end
				array_push($_arrayStatsLogs['logs'][$_nameStats_lastWeek_History], $_yesterdayStat);
			} else {
				// add a stat onto the end
				array_push($_arrayStatsLogs['logs'][$_nameStats_lastWeek_History], $_yesterdayStat);
			}
		}

		$stats->logs[$_nameStats_lastWeek_History] = $_arrayStatsLogs['logs'][$_nameStats_lastWeek_History]; // same day : do nothing

		$counter = 0;

		// Count lastWeek stat
		foreach ($tmp_lastWeek as $stat) {
			$counter += $stat;
		}
		$stats->logs[$_nameStats_lastWeek] = $counter;

		return $stats;
	}
}
?>
