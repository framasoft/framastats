[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Framastats

## Description
Permettre de visualiser rapidement des statistiques sur l'ensemble des services en ligne de Framasoft.    
Le site est visible en ligne à cette adresse : [framastats.org](http://framastats.org/)    
ATTENTION : Cette documentation n'est plus exhaustive. Pour les membres Framasf

## Améliorer le projet sur son ordi
   - Avoir un serveur PHP de type LAMPP/XAMPP 
   - Il est plus simple de retranscrire l'architecture (ou sinon faut changer des variables dans le code > oui y a pas de fichier de conf pour ça, c'est sale mais c'est comme ça =) ) :
    - private/dirForFramastats
    - web/
   - Cloner le projet
    
```bash
    cd private/dirForFramastats/ && git clone git@framagit.org:framasoft/framastats.git framastats
```

   - Télécharger frama nav

```bash
    cd framastats/web/nav && git clone https://framagit.org/framasoft/framanav.git
```
   - Déployer sur le web

```bash
    cd framastats/admin && php deployWeb.php
```
   - Aller vérifier le résultat sur son navigateur préféré (url de type : http://localhost/framastats/web/ )
   - Il ne reste plus qu'à faire des modifs maintenant, puis pousser ;) (oui faut avoir la permission blabla)


## Présentation globale
Le projet se découpe en plusieurs modules/(scripts/parties/bidules) principaux.
   * Un contrôleur principal de constuction des statistiques pour chaque service (service = framadate/framakey/framadate...) : **main.php**
   * Un contrôleur permettant d'enregistrer toutes ces statistiques dans une base de données : **admin/recordAllStats.php**
   * Un contrôleur récupérant et traitant les statistiques dans la base de données pour les fournir à la Vue : **web/scripts/getStats.php**
   * Une vue gérant l'affichage de toutes ces statistiques : dossier **web/**
   * Un contrôleur déployant le dossier web du dossier git au dossier final : **admin/deployWeb.php**

## Installation - création des statistiques pour un nouveau service : main.php
1a) Se placer en ligne de commande dans le dossier du service, dans un endroit non accessible publiquement. (ex : /var/www/nouveauService/private/)   
1b) Créer un dossier qui contiendra tout le dépôt et aussi les fichiers sensibles crées (clés API etc.)  
```bash
    mkdir nameOfYourFramastatsFolder
```
2) Se rendre dans votre dossier et importer les fichiers du dépôt en clonant le dépot
```bash
    cd nameOfYourFramastatsFolder/
    git clone https://framagit.org/framasoft/framastats.git
```
3) Se rendre dans `framastats/` et lancer la commande suivante pour initialiser le nouveau service 
```bash
    cd framastats/
    php main.php nomDuNouveauService init
```
4) Répondre au script pour remplir les informations nécessaires
   * l'url du service : framaNouveau.org/ (paramètre informel) 
   * le chemin vers le dossier web : relative/path/to/the/web/folder
   * le type de base de données utilisées : doit être une bdd présente dans la liste donnée (aller modifier le fichier si plusieurs bdd)   

5) Pour certains modules nécessitant clés, mot de passe... lancer la commande suivante
```bash
    php main.php nomDuNouveauService
```    
Et répondre au script pour lui donner les informations dont le module a besoin.   

6) Ecrire votre code à l'endroit adéquate. Voir [le rôle des fichiers](https://framagit.org/framasoft/framastats/tree/master#le-r-le-des-fichiers).   


## Appel du script
```bash
   php main.php nomDuService (debug)
```
* nomDuService : Framadvd | Framalibre ...   
* debug : non obligatoire -> activera ou non les lignes de debug   

## Intégrer des statistiques à framastats.org
  * On suppose que tu travailles sur ta machine.
  * On suppose que tu veuilles rajouter une catégorie entière de stats, considérons ici que l'on veuille rajouter les statistiques de **Framadvd**.
  * Si tu veux juste rajouter une stat' à une catégorie déjà existante, lis le tutoriel et tu verras bien que certains points ne te concernent pas.

**FONCTIONNEMENT GLOBAL (RAPIDE)**

Les scripts suivants sont situés dans **web/scripts/php** et **web/scripts/js**

  - **PHP - handleStats.php** Appelé régulièrement par un cronjob, lance les 2 prochains scripts:  
  - **PHP - calculateStats.php et calculateChartsStats** Récupère les stats dans la base de données, calcule et transforme les stats
  - **PHP - handleStats.php** Les stats sont propagées en deux fichiers HTML, un pour les stats graphiques et un pour les stats non graphiques
  - **PHP - retrieveCalculatedStats.php** Ces 2 fichiers HTML sont récupérés sur framastats.org et ne sont visibles que dans le code source.
  - **JS - script_index(_Chart).js** Propage les stats au bon endroit dans la page.

**QUELS STATS INTÉGRER ?**
  - Repérer les stat' intéressantes en se rendant au bon endroit.
  - Se rendre dans la base de données de Framastats.
  - Se rendre dans la table framastats_history et rechercher les stats correspondants à Framadvd. Ce sont les noms situés dans la colonne **nameStat** que nous allons manipuler.

**RÉCUPÉRER LES STATISTIQUES DE LA BASE DE DONNÉES (PAS POUR GRAPHE)**

  - Se rendre dans le fichier **web/scripts/php/calculateStats.php**
  - Copier les lignes du __PATTERN__ qui se trouve à la fin
  - Modifier **$tmp_serviceName** avec le nom du service de la base de donnée. Pour nous 'framadvd'
  - Différentes fonctions pour traiter les stats:
      * //handleQuery_oneValue// : récupère la valeur de la stat et la propage 
      * //handleQuery_oneAverageValue// : récupère la moyenne de la stat depuis le début des stats
      * //handleQuery_topValue// : récupère la valeur max de la stat et le jour correspondant
      * //handleQuery_RankTop// : traite certains stats qui sont sous forme de top
      * //handleQuery_lastUpdate// : récupère l'heure de la dernière mise à jour pour un certain service
      * (//handleQuery_RankingService// : classe chaque service selon une stat')
  - Obligatoirement appeler //handleQuery_lastUpdate// pour avoir l'heure de la mise à jour
  - Le plus commun est de boucler pour toutes les stats avec //handleQuery_oneValue//. Utiliser les lignes présentes dans le pattern et modifier en conséquence **$tmp_arrayStat** 
  - Inspire toi de ce qui a été fait pour gérer d'autres stats.

**RÉCUPÉRER LES STATISTIQUES DE LA BASE DE DONNÉES (POUR GRAPHES)**
 
Le but est semblable aux stats non graphiques.  
La particularité vient à la structure de données gérés par la librairie utilisée Chart.js.   
Les stats sont traitées pour être sous le format JSON réclamée. Exemple pour le [graphique ligne ici](http://www.chartjs.org/docs/#line-chart-data-structure).   
Heureusement, si tu n'utilises que ce qui est en place, cela ne te concerne pas ;-)

  - Se rendre dans le fichier **web/scripts/php/calculateChartsStats.php**
  - Copier les lignes du __PATTERN__ qui se trouve à la fin
  - Modifier **$tmp_service** avec le nom du service de la base de donnée. Pour nous 'framadvd'
  - FIXME Actuellement, seuls les graphiques lignes sont gérés grâce à la fonction //handleChartsDatas_DaysAndValues// qui gère les graphiques avec plusieurs jeux de données
  - Penser à fixer la date limite de la première stat voulue (**$tmp_dateFirstStat**)
  - Nommer le graphique (**$tmp_chartName**), c'est le nom qui sera utilisé plus tard dans le fichier index.php
  - Faire un tableau avec le nom de stats (**$tmp_arrayStatsName**). Il est conseillé de placer les stats dans l'ordre de valeur décroissante pour une meilleure gestion des couleurs.
  - Appeler la fonction //handleChartsDatas_DaysAndValues//
  - Recommencer à partir du point 6 pour chaque graphe voulu

__Optionnel :__ 
  - Faire un tableau de tableau avec les options que l'on veut rajouter, notamment les titres des jeux de données si le graphique en contient plusieurs ('label' => 'Nom du jeu de donnée').
  - Les tableaux imbriqués doivent avoir le même ordre que les jeux de données. 
  - Pour savoir ce qui est rajoutable, aller voir la doc, [ici celle des graphes lignes](http://www.chartjs.org/docs/#line-chart-data-structure).

Actuellement, trois couleurs sont utilisées et gérées au maximum. Pour en avoir plus, voir le haut du fichier avec les **$datasetOptions_X** et le tableau les contenant.

**PRÉPARER LE TERRAIN SUR FRAMASTATS.ORG**

Le but va être de créer les panels, div, et autres listes prêtes à accueillir les stats que l'on est allé chercher aux deux étapes précedentes

  - Rends toi au fichier **web/index.php**
  - Copie-colle le pattern de div présent ici : **web/scripts/php/patternDiv.php** à l'endroit voulu dans index.php
  - Amuse toi à changer chaque 'FramaXXX' en 'Framadvd'. Attention, __sois sensible à la casse__ !!
  - Change l'icône, la description, le lien...
  - Chaque sorte de div est présente et commentée pour aider à changer le nécessaire, __lis les commentaires__ !
  - :!: Attention, pour les noms des stats à modifier, ils doivent avoir ce schema là : **framadvd_nomDeLaStatChoisiDansCalculateStats**
  - Utiliser les pattern de div, les copier-coller, les arranger...
  - Changer les constantes dans les fichiers de langue présents ici : **web/lang**
  - Rajouter les liens rapides dans les **Framastats-nav** présentes en haut dans le code (pour grands écrans) et en bas (pour petits écrans)

**CONFIGURER LE JAVASCRIPT**

  - Ajouter (à la fin du fichier) un écouteur pour chaque graphe, appelle //handleChart// avec l'id du graphe, le même nom que celui choisit dans **calculateChartsStats.php**
  - Ajouter un case dans le switch/case avec le nom du graphe
  - Si c'est un graphique avec plusieurs jeux de données, rajouter la ligne //multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"// qui permet d'afficher une légende
  - Possibilité de rajouter d'autres options, voir la doc [général pour tous graphes](http://www.chartjs.org/docs/#getting-started-global-chart-configuration) et la doc [spécifique aux graphes lignes](http://www.chartjs.org/docs/#line-chart-chart-options).

**DÉPLOYER TOUTES VOS MODIFICATIONS**

  - Pousser votre code et venir le tirer sur brunella dans le dossier git
  - Aller dans le dossier admin
  - Déployer votre code en ligne : <php>php deployWeb.php </php>
  - Mettre à jour les stats : <php>cd /path/to/web/scripts/php/ && php handleStats.php</php>
  - Commiter + Pousser le code sur le gitLab pour l'enregistrer - sinon vos modifications pourraient être effacées à cause du git pull qui a lieu à intervalle régulier 
  - Et voilà ! Bon forcement ça ne va pas forcement marcher du premier coup...

**TRUCS A VÉRIFIER SI...**

  * "**...**" est affiché à l'endroit de la stat = Le script JS n'a pas pu déplacé la stat :
     * Vérifier que la stat a bien été récupéré : afficher le code source de la page (CTRL+U). Essayer de repérer la stat en question dans le tas de stats du début.
     * Vérifier que le nom de stat récupéré est identique à celui donné dans la div/span voulu.
  * Le graphe n'affiche rien
     * Vérifier que la stat a bien été récupéré : afficher le code source de la page (CTRL+U). Essayer de repérer la stat en question dans le tas de stats du début.
     * Vérifier que les champs 'labels' et 'datas' ne soient pas vides, si c'est le cas, vérifier la construction dans **calculateChartsStats.php**
     * Vérifier que les noms des stats récupérées soient similaires à celle du canvas, et que celle dans l'écouteur du javascript 
   * Rien ne se passe quand je clique pour afficher le graphique
     * Vérifier dans le script JS **script_index_Chart.js** qu'il y est bien l'écouteur de click et le switch case avec les bons sélecteurs JQuery

**FAQ**
  * "Mais moi je veux juste changer une valeur !?"
    * Et bien pas de problème. Va modifier sa valeur, sa requête ou quoi que ce soit, dans les scripts **calculateChartsStats.php** ou **calculateStats.php** en fonction que ce soit une valeur liée à un graphe. Puis après va lire la sous-partie **DÉPLOYER TOUTES VOS MODIFICATIONS** qui se trouve un peu plus haut.


## Ajouter facilement de nouvelles stats à framastats.org

### TLDR
  - Mettre un fichier JSON de statistiques dans le dossier /path/to/dirFramastats/otherStats. On l'appelle **framatest.json**.  
  - Lancer le bon script ( cd /path/to/dirFramastats/framastats/admin && ./launchWhenNewStatWithFile.sh **framatest** ).    
  - TADAM. Stats ajoutées !     

### Version longue
                                                                                                         
**Dossier /path/to/dirFramastats/otherStats**

Ici se trouvent des fichiers JSON de statistiques de services Framasoft.
1 fichier => 1 service => Multiples statistiques.

Chaque statistique est enregistrée de façon automatique à la base de données framastats grâce à une tâche cron.
Elles peuvent alors être intégrées à framastats.org grâce au script "launchWhenNewStatWithFile.sh" présent dans framastats/admin

**Quel genre de statistiques puis-je rajouter (automatiquement) ?**

Pour l'instant, seulement des statistiques uniques peuvent être rajoutées à framastats   
Exemples :   
[OUI] Une ligne avec marquée : 50 000 photos uploadées   
[NON] Un dropdown contenant plusieurs infos du genre : 42 livres téléchargés ce mois-ci, 5 aujourd'hui etc..     
[OUI] Plein de lignes contenant les mêmes infos (donc c'est juste visuellement, on ne fait pas de dropdown)    
[NON] Un graphe de données dans le temps    

**Quel format de fichier à respecter ?**

Le plus simple est de copier un fichier déjà présent pour en reprendre la structure.
Il faut au moins deux variables:
  -  Le "serviceName"
  -  Une statistique ou les espaces sont remplacés par des _ : "nombre_de_vidéos"

**Comment rajouter des statistiques à framastats ?**

3 étapes :
  - Créer le fichier de statistiques.
  - Aller dans admin/ et lancer la commande ./launchWhenNewStatWithFile.sh
      - Suivre les consignes
  - Vérifier sur framastats !

## Le rôle des fichiers
  * __framastats/__ : Dossier git du projet
     * __main.php__
       * Crée une instance de la classe Utils, c'est ici qu'on choisit le mode debug ou pas.
       * Vérifie l'appel du script
       * Fais compléter les informations manquantes à l'utilisateur si besoin
       * Établit les informations nécessaires pour la suite
       * Appelle scripts/dispatcher.php
     * __scripts/__ : Comporte tous les scripts écrits pour ce projet rangés par "base de données". 
       * __dispatcher.php__
         * Appel le ou les scripts correspondant au(x) service(s) demandé(s)
         * Récupère les stats ainsi créees (classe Stats)
         * Déploie les stats en ligne
             * Transforme les stats d'une classe Stats en format JSON
             * Place les stats dans un dossier tmpStats/ placé au même niveau que le dossier git framastats/
             * Copie ces stats en ligne grâce au chemin indiqué dans pathWebForServices.json
       * __mySQL/__ (Explication valable pour les autres dossiers)
         * __mainScript.php__
             * Établit la connexion à la base de données du service avec les informations enregistrées dans dbInfos.json (au niveau du dossier git)
             * Appelle le fichier de requêtes selon le service
         * __req_framaService.php__
             * Requête précisément la base de donnée du ce service et crée une classe Stats
       * __logsApache/__
       * __piwick/__
       * __socialsNetworks/__ : twitter et facebook
       * __utils/__ avec Util.php comportant un ensemble de fonctions génériques
     * __config/__ : contient des informations publiques expliquées [dans ce paragraphe](https://framagit.org/framasoft/framastats/tree/master#fichiers-de-configuration)
     * __vendor/__ : dossier lié au gestionnaire de paquets Composer
     * __install/__ : getRepoFramastats.php permet d'installer le dossier git rapidement (temporaire)

## Fichiers de configuration
Les informations demandées à l'utilisateur à la première utilisation sont enregistrées puis conservées dans le dossier __config/__.  
Voici une description de chaque fichier potentiellement crée :  

  * __Informations publiques__ dans le git (framastats/config/)
    * urlsForServices.json : indique l'URL où l'on trouvera le fichier de stats (sert de pense-bête pendant le développement)
    * pathWebForService.json : indique le chemin relatif permettant de mettre en ligne le fichier statistics.json
    * databaseForService.json : indique quel(s) type(s) de données le script va devoir gérer. (piwik|mySQL...)
  * __Informations privées__ (enregistrées au niveau du dossier framastats)
    * dbInfos.json : contient les infos nécessaires pour se connecter à une base de données de type mySQL
    * apiKeysConnection_facebook.json : contient les clés nécessaires à l'api rest de facebook
    * apiKeysConnection_twitter.json : contient les clés nécessaires à l'api rest de twitter
    * tokenAuth.json : contient la clé necessaire à l'api rest Piwick

      
## Mises à jour avec Cron
Cronjob rajouté à la /etc/crontab.   
Choix d'une mise à jour du script tous les jours à 22h45 et de lancer le script toutes les X minutes (ici 5 minutes).   

```bash
    45 22   * * *   root    cd /path/to/framastats/ && git pull >/dev/null 2>&1   
    */5 *   * * *   root    cd /path/to/framastats/ && php main.php nomDuFramaService
```

Penser à rajouter tout en haut ton e-mail pour recevoir les résultats du script.

```bash
   MAILTO=ton.adresse@mail.truc
```