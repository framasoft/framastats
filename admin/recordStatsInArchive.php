<?php

/*
 * Record some statistics in database framastats_archive
 *
 * Statistics record in archive are the ones contained in config/statsForArchive.json
 * Limit of days recorded : can be changed in CONSTANTS (see parameter of the main function copyStatFromHistoryToArchive())
 *
 * */

// ################# START TIME ###################
$timeStart	= microtime(true);

// ################## INCLUDE #####################
$pathToUtil_mySQL 	= '../scripts/mySQL/Util_mySQL.php';
$pathToUtil		= '../scripts/utils/Util.php';
include ($pathToUtil_mySQL);
include ($pathToUtil);

// ############### UTILS & DEBUG ##################
$util = new Util();
$util->setDebug("error");

// ################# CONSTANTS ####################
$nbOfArguments		= 1;
$nbOfArgumentsDebug	= $nbOfArguments + 1;
$error			= "### ERROR ###";
$man			= "You should call script as follows :" . PHP_EOL .
			  "> php recordStatsInArchive.php (debug)";
$scriptEnds 		= "### SCRIPT ENDS ###" ;

$urlFramastats          = 'http://framastats.org/statistics.json';
$pathDbInfos		= '../../dbInfos_Framastats.json';
$pathStatsInArchive	= '../config/statsForArchive.json';
$config_keyForAll	= 'all';

$defaultInfo		= 'TO_CHANGE';
$tableFramastats_archive= 'framastats_archive';
$tableFramastats_hstry	= 'framastats_history';
$columnService 		= 'service';
$columnNameStat 	= 'nameStat';
$columnValueStat 	= 'valueStat';
$columnDate 		= 'date';

$limitOfDaysToRecord	= 1; //-1 : no limit

// #################### DATABASE ##################
$dbInfos = array (
	'db_host' 	=> $defaultInfo,
	'db_dbname' 	=> $defaultInfo,
	'db_usr' 	=> $defaultInfo,
	'db_pswrd' 	=> $defaultInfo
	);

$fields_framastats_archive = array (
	'id_stat' => 'int NOT NULL AUTO_INCREMENT',
	'service' => 'varchar(24) NOT NULL',
	'nameStat' => 'varchar(128) NOT NULL',
	'valueStat' => 'varchar(64) NOT NULL',
	'date' => 'date NOT NULL'
	);

// ############## FUNCTIONS ###################

/*
 * Copy stat for the first database (history) to the archive
 * Copy each stat one time per day (take the oldest one in the day)
 *
 * WARNING : Do not copy stats from today
 */
function copyStatFromHistoryToArchive ($dBase, $_service, $_statName, $_limit = -1) {

	// Find the stat
	global $tableFramastats_hstry;
	global $tableFramastats_archive;
	global $columnService;
	global $columnNameStat;
	global $columnValueStat;
	global $columnDate;

	$sql = "SELECT * FROM (
			SELECT $columnValueStat, $columnDate
			FROM $tableFramastats_hstry
			WHERE $columnService LIKE '%" . $_service . "%'
			AND $columnNameStat LIKE '%" . $_statName . "%'
			AND $columnDate NOT LIKE '%00:0%'
			AND DATE(date) < CURDATE()
			ORDER BY $columnDate DESC ) as t1
		GROUP BY DATE($columnDate)
		ORDER BY $columnDate DESC";
	if ($_limit != -1) { $sql .= " LIMIT $_limit"; }

//	echo $sql . PHP_EOL;
	// SELECT STATS
	$res = $dBase->query($sql);

	while ($data = $res->fetch()) {
		$statDate = $data[$columnDate];
		$statValue = $data[$columnValueStat];

//		echo "ICI avec " . $statValue . " --- " . $statDate . PHP_EOL;

		// INSERT STATS into archive database
        	$req = $dBase->prepare("INSERT INTO $tableFramastats_archive(service, nameStat, valueStat, date) VALUES(:service, :nameStat, :valueStat, :date)");
       		 $req->execute(array(
            		'service' => $_service,
            		'nameStat' => $_statName,
            		'valueStat' => $statValue,
	    		'date' => $statDate
           	 ));
	}
}

/*
 * Delete all stats before today
 *
 */
function deleteStatsBeforeToday ($dBase) {
	global $tableFramastats_hstry;
	global $columnDate;

	$sql = "DELETE FROM $tableFramastats_hstry WHERE $columnDate < CURDATE()";
	$dBase->query($sql);
}


// ############## MAIN SCRIPT ###################
// ########### CONNECTION DATABASE ##############
$util->out("### Record All Archive Stats", "info");

$mode = null;

// Check number of arguments
if ($argc < $nbOfArguments || $argc > $nbOfArgumentsDebug) {
	$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
}
// Debug And Init Mode
elseif ($argc == $nbOfArgumentsDebug) {
	$mode = $argv[$nbOfArgumentsDebug-1];
	if ($mode == "debug") {
		$util->setDebug("all");
		$modeUpperCase =  mb_strtoupper($mode);
		$util->out("### $modeUpperCase MODE", "info");
	} else {
		$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
	}
}

// Manage database informations
$finalDbInfos = $util->checkVariablesOrSetThem($pathDbInfos, $dbInfos, $defaultInfo);

// Database connection
try
{
	$dBase = new PDO('mysql:host=' . $finalDbInfos['db_host'] . ';dbname='. $finalDbInfos['db_dbname'] . ';charset=utf8', $finalDbInfos['db_usr'], $finalDbInfos['db_pswrd']);
	$util->out("### Established connection in the database : " . $finalDbInfos['db_dbname'], "success");
}
catch(Exception $e)
{
	$util->out("### Error in database connection : ".$e->getMessage(), "error");
	$util->out("### Check infos in this file : ". $pathDbInfos, "error", true);
}

// ############## RECORD STATS ###################

// Create DbTable Archive
$sql = Util_mySQL::createDbtable($dBase, $tableFramastats_archive, $fields_framastats_archive);
if($sql == -1) {
	$util->out("### Error in creating $tableFramastats_archive", 'error', true); // exit
}

//Retrieving all services with their stat in config file
$arrayEmbeddedServices 		= json_decode(file_get_contents($pathStatsInArchive), true);

// For each service
foreach ($arrayEmbeddedServices as $service => $statForArchive) {

	if ($service == $config_keyForAll) { continue; }

	// Recording stats
	$util->out("Recording " . $service . "...", 'info');
        foreach ($statForArchive as $statName) {
		copyStatFromHistoryToArchive ($dBase, $service, $statName, $limitOfDaysToRecord);
        }
	// Records stats that each service has
	foreach ($arrayEmbeddedServices[$config_keyForAll] as $statName) {
		copyStatFromHistoryToArchive ($dBase, $service, $statName, $limitOfDaysToRecord);
	}
}

// ############ DELETE OLD STATS #################
deleteStatsBeforeToday ($dBase);

// ################# END TIME ###################
$timeEnd 	= microtime(true);
$time		= $timeEnd - $timeStart;
$util->out("### Request took $time seconds", 'info');

?>
